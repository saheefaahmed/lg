/*Example of Linear Gradient Component in React Native*/
import React, {Component} from 'react';
//import React

import {Platform,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  
  TouchableOpacity,
  TextInput,
  StatusBar,
  Image,
  View,
  ImageBackground} from 'react-native';
//import React Native basic components

import LinearGradient from 'react-native-linear-gradient';
//import LinearGradient Componet to make Linear Gradient

export default class App extends Component {
  constructor(props){
    super(props)
    this.state={
      pin1: "",
      pin2: "",
      pin3: "",
      pin4: "",
      pin5: "",
      pin6: "",
    }
  }
  componentDidMount=()=>{
    this.refs.pin1ref.focus()
  }
  render() {
    const { pin1 , pin2, pin3 , pin4 , pin5 , pin6 } =this.state
    return (
      

      <View style={{flex:1, margin:-10 }}>
        <ImageBackground  style={{width: '100%',paddingBottom: 100,height: 150}} source={require('./images/header1.png')}>
        <Text style ={{paddingTop: 25, color: "white", fontFamily: 'Univers', fontStyle: 'italic',fontSize: 30, alignSelf: 'center',}}>Verify </Text>
        </ImageBackground>
        
        <View>
        <Text style ={{color: "black",fontStyle: 'italic',fontSize: 20, alignSelf: 'center',fontWeight: 'bold'}}>Enter Code </Text>
        <Text style ={{color: "black",fontStyle: 'italic',fontSize: 15, alignSelf: 'center',}}>Please enter the verification code </Text>
        <Text style ={{color: "black",fontStyle: 'italic',fontSize: 15, alignSelf: 'center',}}>sent to (number) </Text></View>
      <View style ={{flex: 0.6,paddingTop: 10,justifyContent : "space-evenly", flexDirection:"row"}}>
      
      <TextInput
        ref={"pin1ref"}
        onChangeText={(pin1)=> {
          this.setState({pin1 : pin1})
          if(pin1 != ""){
            this.refs.pin2ref.focus()
          }
        }}
        value={pin1}
        maxLength={1}
        style={styles.ti}
      />
      <TextInput
        ref={"pin2ref"}
        onChangeText={(pin2)=> {
          this.setState({pin2 : pin2})
          if(pin2 != ""){
            this.refs.pin3ref.focus()
          }
        }}
        value={pin2}
        maxLength={1}
        style={styles.ti}
      />
       <TextInput
        ref={"pin3ref"}
        onChangeText={(pin3)=> {
          this.setState({pin3 : pin3})
          if(pin3 != ""){
            this.refs.pin4ref.focus()
          }
        }}
        value={pin3}
        maxLength={1}
        style={styles.ti}
      />
       <TextInput
        ref={"pin4ref"}
        onChangeText={(pin4)=> {
          this.setState({pin4 : pin4})
          if(pin4 != ""){
            this.refs.pin5ref.focus()
          }
        }}
        value={pin4}
        maxLength={1}
        style={styles.ti}
      />
       <TextInput
        ref={"pin5ref"}
        onChangeText={(pin5)=> {
          this.setState({pin5 : pin5})
          if(pin5 != ""){
            this.refs.pin6ref.focus()
          }
        }}
        value={pin5}
        maxLength={1}
        style={styles.ti}
      />
       <TextInput
        ref={"pin6ref"}
        onChangeText={(pin6)=> {
          this.setState({pin6 : pin6})
          if(pin6 != ""){
          
            alert("thanks")
          }
        }}
        value={pin6}
        maxLength={1}
        style={styles.ti}
      /></View>
<View style={styles.container}>
        <Text style ={{color: "#93278f",textDecorationLine: 'underline'}}>Didn't get the code? Resend</Text>
        {/*Simple Gradient*/}
        <LinearGradient 
          colors={['#93278f', '#93278f', '#9400D3']}
          style={styles.linearGradient}>
          <Text style={styles.buttonText}>
            OK
          </Text>
        </LinearGradient>

        </View>

        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
   

  },
  linearGradient: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 100,
    marginTop:16,
    width:100,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Georgia',
    textAlign: 'center',
    margin: 12,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  ti:{backgroundColor: "white", 
  fontWeight: "600", 
  alignSelf: "center", 
  padding: 10, 
  fontSize: 20, 
  height: 55,
  justifyContent:"center",
  width: "12%",
 color: '#93278f',
 borderWidth: 0.9,
  alignContent:"center",
textAlign:"center",



},
  
});